  Siete Y Medio
=================
## Quick Summary ##
This game is a Spanish version of the well-known card game 'Blackjack' or '21.' A player will get to play against dealer, which follows
a certain programmed strategy. A player is initially given 100 credits to play with, and in every round, the player can bet any amount 
within the budget he has. A player can choose either to stay or to receive another card. If his card total exceeds 7.5, he loses the game
and the bet money to the dealer (busted). Otherwise, one who has higher card total wins the round. As a house advantage, if both the player
and the dealer busts, the dealer wins the round. The game ends if the budget goes below 0. 
>(Version 1.0)



## Ranks & Values of Spanish Cards ##

| Name  |   As  | Dos  | Tres  |  Cuatro |  Cinco  |  Seis  |  Siete |   Sota  |  Caballo  |  Rey  |  
|-------|-------|------|-------|---------|---------|--------|--------|---------|-----------|-------|
| Rank  |	1	|  2   |   3   |    4	 |    5	   |   6	|    7	 |   10	   |    11	   |  12   |
| Value | 	1	|  2   |   3   |	4	 |    5	   |   6	|    7	 |   1/2   |    1/2	   |  1/2  |



## Example of plays ##

	You have $100. Enter the bet: 10
	Your cards: 	Rey de espadas (King of spades)
	Your total is 0.5
	Do you want another card (y/n)? y
	New card: 
		Tres de espadas (Three of spades)
	Your cards: 
		Rey de espadas (King of spades)
		Tres de espadas (Three of spades)
	Your total is 3.5
	Do you want another card (y/n)? n
	Dealer's cards: 
		Sota de espadas (Jack of spades)
	The dealer's total is 0.5.
	New card: 
		Siete de copas (Seven of cups)
	Dealer's cards: 
		Sota de espadas (Jack of spades)
		Siete de copas (Seven of cups)
	The dealer's total is 7.5.

	Too bad. You lose $10.

	You have $90.


### Links To External Sources ###

1. [Intro. to Siete Y Medio] : https://en.wikipedia.org/wiki/Sette_e_mezzo
2. [Rules] : https://www.pagat.com/banking/sette_e_mezzo.html

