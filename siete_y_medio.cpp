#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <ctime>
#include <cstdlib>
#include "cards.h"
using namespace std;

// Global constants (if any)
int ini_budget = 100;
int bet;
string response;

// Non member functions declarations (if any)
string player_win(double, double);

// Non member functions implementations (if any)
/* Function uses the logic and decides the result of a round
 @param player : the player's total
 @return whether player won, lost or draw..
 */
string player_win(double player, double dealer){
    //CASE 1 : Both player and dealer bust (house advantage)
    if ((dealer > 7.5) && (player > 7.5)){
        return "LOSE"; // deduct the budget by the bet
    }
    
    //CASE 2 : dealer busts AND player did not bust (player wins)
    else if ((dealer > 7.5) && (player <= 7.5)){
        return "WIN";
    }
    
    // CASE 3 : dealer did not bust AND player bust (dealer wins)
    else if ((dealer <= 7.5) && (player > 7.5)){
        return "LOSE";
    }
    
    // CASE 4 : Both did not bust, but dealer is higher
    else if ((dealer <= 7.5) && (player <= 7.5) && (dealer > player)){
        return "LOSE";
    }
    
    // CASE 5 : Both did not bust, but player is higher
    else if ((dealer) <= 7.5 && (player <= 7.5) && (player > dealer)){
        return "WIN";
    }
    
    // CASE 6 : Both have the same total AND Both did not bust
    else if ((dealer <= 7.5) && (player <= 7.5) && (player == dealer)){
        return "DRAW";
    }
    
    else
        return "";
}

// Stub for main
int main(){
    srand(time(0));
    
    //create a Player object with a starting budget = $100
    Player p1(ini_budget);
    
    while (p1.print_money() > 0) { // Game ends when the budget falls below zero
        do {    // allow only bet limited to the player's budget
            // Tells the user how much he has and take his input for the betting
            cout << "You have $";
            cout << p1.print_money();
            cout << ". Enter the bet: ";
            cin >> bet;
        } while (bet > p1.print_money());
    
        // Game begins. Create the empty hand for the p1
        Hand h1;
        h1.reset();    // make sure every round the hand starts from 0
        Card c1;                // create a new card
        h1.add_card(c1);        // add the created card to the hand
        h1.update_total(c1.get_rank());     // update the player's score
        
        cout << "Your cards: ";
        h1.print_hand();        // output the current hand
        
        cout << "Your total is " << h1.hand_total() << endl;   // tell the score of the current hand
        
        // Get a response for hit or stay. Allow the response only as y or n
        do {
            cout << "Do you want another card (y/n)? ";
            cin >> response;
        }   while ((response != "y") && (response != "n"));
        
        while (response == "y"){    // add cards until he wants to stay (until response == "n")
            Card c2;
            h1.add_card(c2);        // Add a created card c1 to the player 1's hand
            
            cout << "New card: " << endl;
            c2.print_card();
            
            cout << "Your cards: " << endl;
            h1.print_hand();
            
            h1.update_total(c2.get_rank());
            cout << "Your total is " << h1.hand_total() << endl;
            
            do {        // ask for another card
                cout << "Do you want another card (y/n)? ";
                cin >> response;
            }   while ((response != "y") && (response != "n"));
        }
        
        // Dealer's turn
        Hand dealer;
        dealer.reset();
        Card c3;
        dealer.add_card(c3);
        dealer.update_total(c3.get_rank());
        cout << "Dealer's cards: " << endl;
        dealer.print_hand();            // print dealer's hand
        cout << "The dealer's total is " << dealer.hand_total() << "." << endl;  // print dealer's points
        
        while (dealer.hand_total() < 5.5){      // dealer always hits until his total is less than 5.5
            Card c3;
            dealer.add_card(c3);
            dealer.update_total(c3.get_rank());
            cout << "New card: " << endl;
            c3.print_card();
            
            cout << "Dealer's cards: " << endl;
            dealer.print_hand();            // print dealer's hand
            cout << "The dealer's total is " << dealer.hand_total() << "." << endl;  // print dealer's points
        }
        
        if (player_win(h1.hand_total(), dealer.hand_total()) == "WIN"){         // the result of this round is WIN
            p1.update_money((p1.print_money() + bet)); // Increase the budget by the bet
            cout << "\n" << "You win $" << bet << "." << endl << endl;
        }

        else if (player_win(h1.hand_total(), dealer.hand_total()) == "LOSE"){  // the result of this round is LOSE

            p1.update_money((p1.print_money() - bet)); // deduct the budget by the bet
            cout << "\n" << "Too bad. You lose $" << bet << "." << endl << endl;
        }
        else if (player_win(h1.hand_total(), dealer.hand_total()) == "DRAW"){   // the result of this round is DRAW
            p1.update_money(p1.print_money());  // No change
            cout << "\n" << "Nobody wins!" << endl << endl;
        }
    }
   return 0;
}
