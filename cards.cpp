#include "cards.h"
#include <cstdlib>
#include <iostream>
#include <time.h>

/*
 You might or might not need these two extra libraries
 #include <iomanip>
 #include <algorithm>
 */

/* *************************************************
 Card class
 ************************************************* */

/*
 Default constructor for the Card class.
 It could give repeated cards. This is OK.
 Most variations of Blackjack are played with
 several decks of cards at the same time.
 */
Card::Card(){
    int r = 1 + rand() % 4;
    switch (r) {
        case 1: suit = OROS; break;
        case 2: suit = COPAS; break;
        case 3: suit = ESPADAS; break;
        case 4: suit = BASTOS; break;
        default: break;
    }
    
    r = 1 + rand() % 10;
    switch (r) {
        case  1: rank = AS; break;
        case  2: rank = DOS; break;
        case  3: rank = TRES; break;
        case  4: rank = CUATRO; break;
        case  5: rank = CINCO; break;
        case  6: rank = SEIS; break;
        case  7: rank = SIETE; break;
        case  8: rank = SOTA; break;
        case  9: rank = CABALLO; break;
        case 10: rank = REY; break;
        default: break;
    }
}

// Accessor: returns a string with the suit of the card in Spanish
string Card::get_spanish_suit() const {
    string suitName;
    switch (suit) {
        case OROS:
            suitName = "oros";
            break;
        case COPAS:
            suitName = "copas";
            break;
        case ESPADAS:
            suitName = "espadas";
            break;
        case BASTOS:
            suitName = "bastos";
            break;
        default: break;
    }
    return suitName;
}

// Accessor: returns a string with the rank of the card in Spanish
string Card::get_spanish_rank() const {
    string rankName;
    switch (rank) {
        case AS:
            rankName = "As";
            break;
        case DOS:
            rankName = "Dos";
            break;
        case TRES:
            rankName = "Tres";
            break;
        case CUATRO:
            rankName = "Cuatro";
            break;
        case CINCO:
            rankName = "Cinco";
            break;
        case SEIS:
            rankName = "Seis";
            break;
        case SIETE:
            rankName = "Siete";
            break;
        case SOTA:
            rankName = "Sota";
            break;
        case CABALLO:
            rankName = "Caballo";
            break;
        case REY:
            rankName = "Rey";
            break;
        default: break;
    }
    return rankName;
}



// Accessor: returns a string with the suit of the card in English
// This is just a stub! Modify it to your liking.
string Card::get_english_suit() const {
    string suitName_eng;
    switch(suit){
        case OROS:
            suitName_eng = "coins";
            break;
        case COPAS:
            suitName_eng = "cups";
            break;
        case ESPADAS:
            suitName_eng = "spades";
            break;
        case BASTOS:
            suitName_eng = "clubs";
            break;
        default: break;
    }
    return suitName_eng;
}

// Accessor: returns a string with the rank of the card in English
// This is just a stub! Modify it to your liking.
string Card::get_english_rank() const {
    string rankName_eng;
    switch(rank){
        case AS:
            rankName_eng = "Ace";
            break;
        case DOS:
            rankName_eng = "Two";
            break;
        case TRES:
            rankName_eng = "Three";
            break;
        case CUATRO:
            rankName_eng = "Four";
            break;
        case CINCO:
            rankName_eng = "Five";
            break;
        case SEIS:
            rankName_eng = "Six";
            break;
        case SIETE:
            rankName_eng = "Seven";
            break;
        case SOTA:
            rankName_eng = "Jack";
            break;
        case CABALLO:
            rankName_eng = "Knight";
            break;
        case REY:
            rankName_eng = "King";
            break;
        default: break;
    }
    return rankName_eng;
}


// Assigns a numerical value to card based on rank.
// AS=1, DOS=2, ..., SIETE=7, SOTA=10, CABALLO=11, REY=12
int Card::get_rank() const {
    return static_cast<int>(rank) + 1 ;
}

// Comparison operator for cards
// Returns TRUE if card1 < card2
bool Card::operator < (Card card2) const {
    return rank < card2.rank;
}

// Output the card in the desirable format
void Card::print_card() const{
    // Spanish version of card
    cout << "\t" << get_spanish_rank() << " de " << get_spanish_suit();
    // English version of card
    cout << " (" << get_english_rank() << " of " << get_english_suit() << ")" << endl;
}




/* *************************************************
   Hand class
   ************************************************* */
// Implemente the member functions of the Hand class here.

/* Default constructor
 create a default hand - empty vector of cards
 */
Hand::Hand(){
    cur = vector<Card>();
}

/* Add a input card to the cur vector of Hand class
 /@param c a card to be put in the Hand vector
 */
void Hand::add_card(Card c){
    cur.push_back(c);
}

// Calculate the player hand's total
void Hand::update_total(double n){
    if (n > 9){    // if Sota, Caballo or Rey, value = 1/2
        cur_total += 0.5;
    }
    else {         // otherwise, the value = input, which is the rank of the card
        cur_total += n;
    }
}

// reset the hand for a new round
void Hand::reset(){
    cur_total = 0;
}

// Accessor: output the total score of the current hand
double Hand::hand_total(){
    return cur_total;
}

// use the print_card function to output a vector of Cards
void Hand::print_hand(){
    for (const auto i : cur){
        i.print_card();
    }
}

/* *************************************************
   Player class
   ************************************************* */
// Implemente the member functions of the Player class here.


// One parameter constructor
//@param : n is the starting budget of the player
Player::Player(int n){
    money = n;
}

// Accessor
//@return the money private variable
int Player::print_money(){
    return money;
}

// Assign the input to the money variable
//@param new_money : this integer is assigned to the Player class private variable, money
void Player::update_money(int new_money){
    money = new_money;
}
